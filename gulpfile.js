var gulp = require('gulp');
var browser = require('browser-sync');

gulp.task('default', function() {
    browser.init({
      server: './',
      open: false
    });
});

gulp.task('watch', ['default'], function() {
    gulp.watch('*.{html,js,css}', ['reload']);
});

gulp.task('reload', function() {
    browser.reload();
});
