'use strict';

(function ($) {
  superplaceholder({
    el: $('input').get(0),
    sentences: [
      'یک عدد وارد کنید',
      '1000',
      '532000',
      '42246855520'
    ],
    options: {
      cursor: '',
      loop: true,
      startOnFocus: false,
    }
  })
  
  $('input').on('keyup change', function (ev) {
    let val = $(this).val();
    if ( val == '' ) {
      $('.result').text('یک عدد بالا بنویسید');
    } else if (parseInt(val) != NaN) {
      let result = ThreeDigitToText(val);
      if (result != null)
        $('.result').text(result);
      else
        $('.result').text('هنوز این عدد رو بلد نیستم.');
    } else {
      $('.result').text('عددی که وارد کردی صحیحی نیست ها!!');
    }

  });

  function ThreeDigitToText(inputNumber) {
    let dic = [
      'صفر',
      'یک',
      'دو',
      'سه',
      'چهار',
      'پنج',
      'شش',
      'هفت',
      'هشت',
      'نه',
      'ده',
      'یازده',
      'دوازده',
      'سیزده',
      'چهارده',
      'پانزده',
      'شانزده',
      'هفده',
      'هجده',
      'نوزده',
      'بیست',
      'سی',
      'چهل',
      'پنجاه',
      'شصت',
      'هفتاد',
      'هشتاد',
      'نود',
      'صد',
      'دویست',
      'سیصد',
      'چهارصد',
      'پانصد',
      'ششصد',
      'هفتصد',
      'هشتصد',
      'نهصد',
      'هزار', // 6Digit
      'میلیون', // 9Digit
      'میلیارد', // 12Digit
      'بیلیون', // 15Digit
      'بیلیارد', // 18Digit
      'تریلیون', // 21Digit
      'تریلیارد', // 24Digit
      'کوآدریلیون', // 27Digit
      'کادریلیارد', // 30Digit
      'کوینتیلیون', // 33Digit
      'کوانتینیارد', // 36Digit
      'سکستیلیون', // 39Digit
      'سکستیلیارد', // 42Digit
      'سپتیلیون', // 45Digit
      'سپتیلیارد', // 48Digit
      'اکتیلیون', // 51Digit
      'اکتیلیارد', // 54Digit
      'نانیلیون', // 57Digit
      'نانیلیارد', // 60Digit
      'دسیلیون', // 63Digit
      'دسیلیارد', // 66Digit
    ];

    inputNumber = inputNumber.toString();

    // Strip lead zeroes
    const regex = /^0*(0|[1-9]*[0-9]+)/;
    let matches = regex.exec(inputNumber);

    if (matches !== null) {
        // The result can be accessed through the `m`-variable.
        inputNumber = matches[1];
    } else {
      return;
    }
    console.log(inputNumber);
    
    
    /** @var int Input Length */
    let length = inputNumber.length;

    /** @var string Output String */
    let output = '';
    
    if (length > (dic.length - 37) * 3 + 3) return null;


    for (let index = 0; index < inputNumber.length; index++) {
      let indexNumber = inputNumber[index];
      if (index > 0 && indexNumber != 0) {
        output += ' و '
      }

      if (index == length - 1) {
        if (indexNumber != 0 || (indexNumber == 0 && length == 1)) {
          output += dic[indexNumber];
        }
      } else if (index == length - 2) {
        if (indexNumber == '0') {
          output += '';
        } else if (indexNumber == '1') {
          index++;
          indexNumber = inputNumber[index];
          output += dic[10 + parseInt(indexNumber)];
        } else {
          output += dic[20 + parseInt(indexNumber) - 2];
        }
      } else if (index == length - 3) {
        if (indexNumber == '0') {
          output += '';
        } else {
          output += dic[27 + parseInt(indexNumber)];
        }
      } else {
        let pos = parseInt((length - index - 1) / 3);
        let threeDigit = '',
          i;
        for (i = 0; i <= (length - index - 1) % 3; i++) {
          threeDigit += inputNumber[i + index];
        }
        index += i - 1;
        let tmpOutput = ThreeDigitToText(threeDigit);
        if (tmpOutput != '') {
          output += tmpOutput + ' ' + dic[36 + pos];
        }

      }

    }

    return output;
  }

})(jQuery);